############## Getting out the required outputs ##############
output "bucket_arn"{
    value = aws_s3_bucket.bucket.arn
}
output "role_arn"{
    value = aws_iam_role.firehose_role[0].arn
}
output "zookeeper_connect_string" {
  value = aws_msk_cluster.mftkafka.zookeeper_connect_string
}

output "bootstrap_brokers_tls" {
  description = "TLS connection host:port pairs"
  value       = aws_msk_cluster.mftkafka.bootstrap_brokers_tls
}
