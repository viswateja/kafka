variable "kafka_subnet_1" {
  description = "Subnet for kafka"
  type        = string
  default     = "subnet-530e1029"
}
variable "vpc_create" {
  description = "vpc variable for the cidr blocks"
  type = string
  default = false
}
variable "kafka_subnet_2" {
  description ="Subnet for kafka"
  type        =string
  default     ="subnet-d8c48294"
}
variable "kafka_subnet_3" {
  description ="Subnet for kafka"
  type        =string
  default     ="subnet-edc36386"
}
variable "security_group_id" {
  description ="vpc security group ids for msk"
  type        =string
  default     ="sg-1b2baa6b"
}
variable "kafka_cluster_name" {
    type    = string
    default = "mftkafka"
}

variable "kafka_version" {
    type    = string
    default = "2.4.1.1"
}

variable "no_of_broker_nodes" {
    type    = string
    default = "3"
}

variable "broker_instance_type" {
    type    = string
    default = "kafka.m5.large"
}

variable "broker_ebs_size" {
    type    = string
    default = "10"
}
variable "cidr" {
    type    = string
    default = "192.168.0.0/22"
}
variable "firehose_iam_role" {
    default = "false"
}
variable "cloudwatch_log_group_create" {
    default = "true"
}
variable "s3_bucket_create" {
    default = "false"
}
variable "iam_role_create" {
    default = "true"
}
