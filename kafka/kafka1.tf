#resource "aws_vpc" "vpc" {
 # cidr_block = var.cidr
 # }
data "aws_availability_zones" "azs" {
  state = "available"
}
#resource "aws_kms_key" "kms" {
 # description = "example"
#}
resource "aws_cloudwatch_log_group" "test" {
  count     = var.cloudwatch_log_group_create ? 1 : 0
  name      = "msk_broker_logs"
}
resource "aws_s3_bucket" "bucket" {
   bucket = "pod7-msk-broker-logs-bucket"
  acl    = "private"
}
#resource "aws_iam_role" "firehose_role" {
 # count = var.iam_role_create ? 1 : 0
  #name = "firehose_test_role"

  #assume_role_policy = <<EOF
#{#
#"Version": "2012-10-17",
#"Statement": [
 # {
  #  "Action": "sts:AssumeRole",
   # "Principal": {
    #  "Service": "firehose.amazonaws.com"
   # },
   # "Effect": "Allow",
   # "Sid": ""
 # } 
 # ]
#} 
#EOF
#}
#resource "aws_kinesis_firehose_delivery_stream" "test_stream" {
 # name        = "terraform-kinesis-firehose-msk-broker-logs-stream"
 # destination = "s3"

  #s3_configuration {
   # role_arn   = aws_iam_role.firehose_role[0].arn
    #bucket_arn = aws_s3_bucket.bucket.arn
 # }

  #tags = {
   # LogDeliveryEnabled = "placeholder"
 # }

  #lifecycle {
   # ignore_changes = [
    #  tags["LogDeliveryEnabled"],
   # ]
 # }
#}
resource "aws_msk_cluster" "mftkafka" {
  cluster_name           = var.kafka_cluster_name
  kafka_version          = var.kafka_version
  number_of_broker_nodes = var.no_of_broker_nodes

  broker_node_group_info {
    instance_type   = var.broker_instance_type
    ebs_volume_size = var.broker_ebs_size
    client_subnets = [var.kafka_subnet_1,var.kafka_subnet_2,var.kafka_subnet_3]  
    security_groups = [var.security_group_id]
  }
 # encryption_info {
  #  encryption_at_rest_kms_key_arn = aws_kms_key.kms.arn
 # }

  #open_monitoring {
   # prometheus {
    #  jmx_exporter {
     #   enabled_in_broker = true
     # }
      #node_exporter {
       # enabled_in_broker = true
     # }
    #}
 # }

  logging_info {
    broker_logs {
      cloudwatch_logs {
        enabled   = true
        log_group = aws_cloudwatch_log_group.test[0].name
      }
      # firehose {
      #   enabled         = true
      #   delivery_stream = ""#aws_kinesis_firehose_delivery_stream.test_stream[0].name
      # }
      s3 {
         enabled = true
 #        bucket  = try(aws_s3_bucket.bucket[0].arn, "")
         bucket = aws_s3_bucket.bucket.id
         prefix  = "logs/msk-"
       }
    }
  }

  tags = {
    env = "Pod-7"
  }
}
